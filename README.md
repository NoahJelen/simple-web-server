# Simple Web Server
![](icon.png)

A web server, just without all the complexity of setting one up!

## Template Page
![](template_page.png)

## How to use

`sws launch [server name]`: Launch a web server

`sws list`: List all known web servers

`sws new [server name] [port number] <directory>`: Create a new web server with specified name and port number optionally in a specified directory

`sws remove [server name]`: Remove a web server (Be careful! This cannot be undone!)

## Planned Features:
- [ ] HTTPS support
- [ ] Python support
- [ ] Relational database support (SQLite maybe?)