#!/system/bin/sh
# Build script for Termux
rm $PREFIX/bin/sws
cargo clean
cargo build --release
cp target/release/sws $PREFIX/bin/sws
echo "Installed Simple Web Server."
