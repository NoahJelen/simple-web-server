use actix_files::Files;
use actix_web::{
    web,
    http::header,
    App,
    HttpServer,
    HttpResponse,
    dev::Service,
};
use rust_utils::{config::Config, logging::Log};
use std::{env, process, fs};
use lazy_static::lazy_static;

mod config;
use config::ServerConfig;

lazy_static! {
    static ref LOG: Log = {
        let args: Vec<String> = env::args().collect();
        let def_name = "server".to_string();
        let name = args.get(2).unwrap_or(&def_name);
        Log::get(name, "simple-web-server")
    };
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    LOG.report_panics(true);
    ctrlc::set_handler(|| {
        LOG.line_basic("Shutting down...", true);
        process::exit(0);
    }).expect("Error setting Ctrl-C handler");

    // get the command line arguments of the program
    let args: Vec<String> = env::args().collect();
    
    let mut config = ServerConfig::load();

    if let Some(cmd) = args.get(1) {
        match cmd.as_str() {
            // create a new web server
            "new" => {
                if let Some(name) = args.get(2) {
                    println!("Creating {name} web server...");
                    let directory = args.get(4).map(|s| s.to_string()).unwrap_or(format!("{}/{name}", config.default_dir));
                    if let Some(port_str) = args.get(3) {
                        if let Ok(port) = port_str.parse::<u16>() {
                            if args.get(4).is_none() {
                                if env::set_current_dir("template/").is_err() {
                                    env::set_current_dir("/usr/share/simple-web-server/template/")?;
                                }
                                fs::create_dir_all(&directory).unwrap();
                                fs::copy("404.html", format!("{directory}/404.html")).unwrap();
                                fs::copy("backdrop.png", format!("{directory}/backdrop.png")).unwrap();
                                fs::copy("banner.png", format!("{directory}/banner.png")).unwrap();
                                fs::copy("favicon.ico", format!("{directory}/favicon.ico")).unwrap();
                                fs::copy("favicon.png", format!("{directory}/favicon.png")).unwrap();
                                fs::copy("index.html", format!("{directory}/index.html")).unwrap();
                                fs::copy("logo.png", format!("{directory}/logo.png")).unwrap();
                                fs::copy("styles.css", format!("{directory}/styles.css")).unwrap();
                            }

                            config.instances.push(config::ServerInstance {
                                name: name.to_string(),
                                port,
                                directory
                            });

                            config.save().unwrap();
                            println!("Successfully created {name} web server");
                        }
                        else {
                            eprintln!("Invalid port number!");
                            process::exit(101);
                        }
                    }
                    else {
                        eprintln!("Port number is missing!");
                        process::exit(101);
                    }

                    return Ok(());
                }
                else {
                    eprintln!("Name is missing!");
                    process::exit(101);
                }
            },

            // launch a web server
            "launch" => {
                if let Some(name) = args.get(2) {
                    LOG.line_basic(format!("Starting {name} web server..."), true);
                    if let Some(instance) = config.get_instance(name) {
                        let server_instance = instance.clone();
                        let directory = instance.directory.clone();
                        LOG.line_basic(format!("Startup complete."), true);
                        HttpServer::new(move ||
                            App::new()
                                .wrap_fn(|req, srv| {
                                    LOG.line_basic("Executing client request...", true);
                                    let head = req.head();
                                    if let Some(addr) = head.peer_addr {
                                        LOG.line_basic(format!("Client address: {}:{}", addr.ip(), addr.port()), true);
                                    }
                                    LOG.line_basic(format!("Request method: {}", head.method.as_str()), true);
                                    LOG.line_basic(format!("Requested resource: {}", head.uri), true);
                                    if let Some(uagent) = head.headers.get(header::USER_AGENT) {
                                        LOG.line_basic(format!("User Agent: {}", uagent.to_str().unwrap()), true);
                                    }
                                    
                                    let fut = srv.call(req);
                                    async {
                                        let res = fut.await?;
                                        LOG.line_basic("Response sent", true);
                                        Ok(res)
                                    }
                                })

                                .service(
                                    Files::new("/", &directory)
                                        .redirect_to_slash_directory()
                                        .index_file("index.html")
                                )
                                .app_data(web::Data::new(server_instance.clone()))
                                .default_service(web::to(not_found_page))
                        )
                            .bind(("0.0.0.0", instance.port))?
                            .run()
                            .await
                    }
                    else {
                        eprintln!("{name} web server does not exist!");
                        process::exit(101);    
                    }
                }
                else {
                    eprintln!("Name is missing!");
                    process::exit(101);
                }
            },

            // delete a web server
            "remove" => {
                if let Some(name) = args.get(2) {
                    println!("Creating {name} web server...");
                    if let Some(instance) = config.get_instance(&name) {
                        let directory = &instance.directory;
                        if env::set_current_dir("template/").is_err() {
                            env::set_current_dir("/usr/share/simple-web-server/template/")?;
                        }
                        fs::remove_dir_all(&directory).unwrap_or(());
                    }
                    config.instances.retain(|i| i.name != *name);
                    config.save().unwrap();
                    println!("Successfully removed {name} web server");
                    return Ok(());
                }
                else {
                    eprintln!("Name is missing!");
                    process::exit(101);
                }
            },

            // list all web servers
            "list" => {
                println!("All Web Servers:");
                for server in &config.instances {
                    println!("\nName: {}", server.name);
                    println!("Directory: {}", server.directory);
                }
                Ok(())
            }

            _ => {
                eprintln!("Invalid command!");
                process::exit(101);
            }
        }
    }
    else {
        eprintln!("Please specify a command!");
        process::exit(101);
    }
}

async fn not_found_page(instance: web::Data<config::ServerInstance>) -> HttpResponse {
    let default_response =
        "<!DOCTYPE html>\n\
        <html lang=\"\">\n\
        <head>\n\
            <title>404 Not Found</title>
        </head>\n\
        <body>\n\
            <h1>404 Not Found</h1>\n\
            <a href=\"/\">Main Page</a>\n\
        </body>";
    let file_404 = fs::read_to_string(format!("{}/404.html", instance.directory)).unwrap_or(default_response.to_string());
    HttpResponse::NotFound()
        .content_type("text/html")
        .body(file_404)
}