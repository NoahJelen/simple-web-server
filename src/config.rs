use std::env;
use serde_derive::{Serialize, Deserialize};
use rust_utils::config::Config;

#[derive(Serialize, Deserialize, Clone)]
pub struct ServerInstance {
    pub name: String,
    pub directory: String,
    pub port: u16,
}

#[derive(Serialize, Deserialize)]
pub struct ServerConfig {
    pub default_dir: String,
    pub instances: Vec<ServerInstance>
}

impl ServerConfig {
    pub fn get_instance(&self, name: &str) -> Option<&ServerInstance> {
        for instance in &self.instances {
            if instance.name == name {
                return Some(instance);
            }
        }
        None
    }
}

impl Default for ServerConfig {
    fn default() -> ServerConfig {
        let default_dir = env::var("HOME").expect("Where the hell is your home folder?!");
        ServerConfig { default_dir, instances: vec![] }
    }
}

impl Config for ServerConfig {
    const FILE_NAME: &'static str = "config.toml";
    fn get_save_dir() -> String {
        format!("{}/.config/simple-web-server", env::var("HOME").expect("Where the hell is your home folder?!"))
    }
}