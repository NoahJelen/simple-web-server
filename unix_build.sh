#!/bin/sh
# This should theoretically run on any unix-like system (including macOS)
rm $HOME/.local/bin/sws
cargo clean
cargo build --release
cp target/release/sws $HOME/.local/bin/sws
echo "Installed Simple Web Server."
echo "make sure $HOME/.local/bin/ is in your \$PATH variable!"
